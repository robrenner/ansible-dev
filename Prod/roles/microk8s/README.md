# README for MicroK8s role

Role to install and setup Microk8s.

### Issues

Currently we have in the role the following items that may have better implementation methods:

* hard coded username
* predefined and hard coded add-on enablement
* simplified proxy setup for the dashboard, find a better way to do it

In general for microk8s we also need to further learn to understand Services and Ingress.

### Alias

MicroK8s uses a namespaced kubectl command to prevent conflicts with any existing installs of kubectl. If you don’t have an existing install, it is easier to add an alias (append to ~/.bash_aliases) like this:  ```alias kubectl='microk8s kubectl'```

### Addons

Dashboard: The standard Kubernetes Dashboard
DNS: facilitate communication between services
Storage: Create a default storage class which allocates storage from a host directory.
Ingress: NGINX Ingress Controller
Ambassador: Gateway API Ingress
Juju:  Enables a juju client to work with MicroK8s.

#### Dashboard

Once installed, will need to configure way to access it.

kubectl proxy: By default it is only accessible locally from the machine that created it.
kubectl port-forward:
NodePort: For development on single-node cluster setup (modify the Dashboard service yaml)
Ingress: 

https://microk8s.io/docs/addon-dashboard
https://github.com/kubernetes/dashboard/tree/master/docs/user/accessing-dashboard

#### Ingress

https://microk8s.io/docs/addon-ingress
Example:

apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: http-ingress
spec:
  rules:
  - http:
      paths:
      - path: /
        backend:
          serviceName: some-service
          servicePort: 80

#### Example Ambassador

https://microk8s.io/docs/addon-ambassador

cat<<EOF | microk8s.kubectl apply -f -
---
kind: Pod
apiVersion: v1
metadata:
  name: foo-app
  labels:
    app: foo
spec:
  containers:
  - name: foo-app
    image: hashicorp/http-echo:0.2.3
    args:
    - "-text=foo"
---
kind: Service
apiVersion: v1
metadata:
  name: foo-service
spec:
  selector:
    app: foo
  ports:
  # Default port used by the image
  - port: 5678
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: example-ingress
  annotations:
    kubernetes.io/ingress.class: ambassador
spec:
  rules:
  - http:
      paths:
      - path: /foo
        backend:
          serviceName: foo-service
          servicePort: 5678
EOF
